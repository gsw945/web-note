# -*- coding: utf-8 -*-
import os


def get_by_reference(reference, path_rule):
    '''根据参考路径，得出目录规则对应的路径'''
    path_rule = path_rule.strip()
    path_rule = path_rule.strip('/')
    arr = path_rule.split('/')
    arr.reverse()

    reference = os.path.abspath(reference)
    reference = os.path.dirname(reference)
    for item in arr:
        if item == '.':
            pass
        elif item == '..':
            reference = os.path.dirname(reference)
        else:
            reference = os.path.join(reference, item)
            reference = os.path.realpath(reference)
            reference = os.path.abspath(reference)
    return reference

root_dir = get_by_reference(__file__, '..')

static_dir = os.path.join(root_dir, 'frontend/static')
templates_dir = os.path.join(root_dir, 'frontend/templates')

upload_dir = os.path.join(root_dir, 'upload')
if not os.path.exists(upload_dir):
    os.makedirs(upload_dir)

datas_dir = os.path.join(root_dir, 'datas')
if not os.path.exists(datas_dir):
    os.makedirs(datas_dir)

datas_blob_dir = os.path.join(datas_dir, 'blob_zodb')