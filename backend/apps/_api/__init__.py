# -*- coding: utf-8 -*-
import re
import json

from bottle import (
    Bottle,
    request,
    response,
    abort
)


api_app = Bottle()

from . import _folder
from . import _document