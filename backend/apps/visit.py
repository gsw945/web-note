# -*- coding: utf-8 -*-
from bottle import (
    Bottle,
    request
)


visit_app = Bottle()


@visit_app.route('/')
def visit_home():
    return 'hello [visit-home]'