# -*- coding: utf-8 -*-
from bottle import (
    Bottle,
    request,
    jinja2_template,
    static_file
)


admin_app = Bottle()


@admin_app.route('/')
def admin_home():
    return jinja2_template('admin/pages/index.jinja2')