# -*- coding: utf-8 -*-
import os
import json

from .. import (
    get_by_reference,
    datas_dir,
    datas_blob_dir
)

config_file = get_by_reference(__file__, './config.json')
zeo_config = json.load(open(config_file, 'r'))
zeo_config['path'] = os.path.join(datas_dir, 'data.zodb')
zeo_config['blob_dir'] = datas_blob_dir
if not os.path.exists(zeo_config['blob_dir']):
    os.makedirs(zeo_config['blob_dir'])

__all__ = [zeo_config]