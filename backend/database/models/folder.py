# -*- coding: utf-8 -*-
from anytree import Node

from ._base import Base


# http://www.zodb.org/en/latest/guide/writing-persistent-objects.html
class Folder(Base, Node):
    """docstring for Folder"""
    @staticmethod
    def tree_key():
        return 'tree'

    @staticmethod
    def tree_root_key():
        return 'tree-root'

    def __init__(self, name, parent=None):
        super(Folder, self).__init__(name, parent)
        self.name = name.encode('utf-8')
        self.parent = parent
        self._p_changed = True

    def toDict(self):
        return {
            'id': self.id_bin2hex(),
            'name': self.name.decode('utf-8'),
            'is_leaf': self.is_leaf
        }


def main_demo():
    from anytree import RenderTree
    root = Folder('root')
    c1 = Folder('child-1', parent=root)
    c2 = Folder('C-2', parent=root)
    s12 = Folder('son-1-2', parent=c1)
    s11 = Folder('son-1-1', parent=c1)
    s21 = Folder('son-2-1', parent=c2)
    # delete Node
    s21.parent = None
    del s21
    print(root)
    print(s12)
    def my_sort(items):
        return sorted(items, key=lambda item: item.name)
    for pre, fill, node in RenderTree(root, childiter=my_sort):
        print('{0}{1}'.format(pre, node.name.decode('utf-8')))

if __name__ == '__main__':
    main_demo()