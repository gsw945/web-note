# -*- coding: utf-8 -*-
import hashlib
import binascii

from persistent import Persistent


class Base(Persistent):
    """docstring for Base"""
    def get_hash(self):
        md5 = hashlib.md5()
        md5.update(self._p_oid)
        return md5.hexdigest()

    def id_bin2hex(self):
        '''binary转成对应的hex字符串，便于传输'''
        return binascii.b2a_hex(self._p_oid).decode('ascii', 'ignore')

    @staticmethod
    def id_hex2bin(hex_id):
        '''hex字符串转成对应的binary，用于查询计算'''
        return binascii.a2b_hex(hex_id.encode('ascii'))