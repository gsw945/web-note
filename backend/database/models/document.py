# -*- coding: utf-8 -*-
from ._base import Base

from BTrees.OOBTree import TreeSet


class Document(Base):
    """docstring for Document"""
    @staticmethod
    def book_key():
        return 'book'

    def __init__(self, title, content, parent=None, tags=None):
        super(Document, self).__init__()
        self.title = title
        self.content = content
        self.parent = parent
        if tags is None:
            self.tags = TreeSet()

    def toDict(self):
        return {
            'id': self.id_bin2hex(),
            'title': self.title,
            'content': self.content,
            'tags': [item for item in self.tags] if bool(self.tags) else [],
            'parent': self.parent.id_bin2hex() if not self.parent is None else None
        }