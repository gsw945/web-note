# -*- coding: utf-8 -*-
import ZEO

from . import zeo_config


address, stop = ZEO.server(**zeo_config)
# print('{0}:{1}'.format(*address))

try:
    while True:
        pass
except (KeyboardInterrupt, SystemExit):
    print('stoping...')
finally:
    stop()