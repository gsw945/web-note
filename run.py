# -*- coding: utf-8 -*-
from backend.apps import (
    main_app,
    setup_db
)

import bottle

from backend.database import client


db_client = client.MyClient()
setup_db(db_client)

params = {
    'host': '0.0.0.0',
    'port': 8080,
    'debug': True,
    'reloader': True,
    'server': 'waitress',
    # 'server': 'auto',
    'app': main_app
}
bottle.run(**params)