function getDocList(folder) {
    var _data = {};
    if(folder) {
        _data['parent'] = folder;
    }
    $.ajax({
        url: '/api/document/',
        type: 'GET',
        data: _data,
        dataType: 'json',
        success: function(data) {
            // console.log(data)
            if(data.error == 0) {
                showDocList(data.data);
            }
            else {
                layer.msg(data.desc, {
                    icon: 0,
                    time: 2800 //2.8秒关闭
                }, function() {
                    //do something
                });
            }
        },
        error: function(xhr) {
            console.log(xhr.responseText);
        },
        complete: function() {
            //
        }
    });   
}

function saveNewDoc(title, content, folder) {
    if(!title || title.length < 1) {
        layer.msg('标题必填', {
            icon: 0,
            time: 2800 //2.8秒关闭
        }, function() {
            //do something
        });
        return;
    }
    var _data = {
        title: title,
        content: content
    };
    if(folder) {
        _data['parent'] = folder;
    }
    $.ajax({
        url: '/api/document/',
        type: 'POST',
        data: _data,
        dataType: 'json',
        success: function(data) {
            console.log(data)
            if(data.error == 0) {
                //
            }
            else {
                //
            }
        },
        error: function(xhr) {
            console.log(xhr.responseText);
        },
        complete: function() {
            //
        }
    });
}

function short_content(content) {
    var text = $('<div />').append(content).text().trim().substr(0, 250);
    return text;
}

function generate_tags(tags) {
    console.log('TODO: show tags');
    return ''
}

function showDocList(data) {
    // console.log(data)
    var $elem = $('#doc-list');
    $elem.empty();
    if(data.length > 0) {
        for (var i = 0; i < data.length; i++) {
            var $item = $([
                '<div data-id="', data[i].id, '">',
                    '<div class="line-title">',
                        '<strong>', data[i].title, '</strong>',
                    '</div>',
                    '<div class="line-content">',
                        '<span>', short_content(data[i].content), '</span>',
                        '<a data-content="">&gt;&gt;&gt;show more</a>',
                    '</div>',
                    '<div class="line-tags">',
                        '<strong>', generate_tags(data[i].tags), '</strong>',
                    '</div>',
                '</div>'
            ].join(''));
            $item.find('.line-content > a[data-content]').attr('data-content', data[i].content);
            $elem.append($item);
        }
    }
    else {
        $elem.html('<div class="list-empty"><span>文档列表为空</span></div>');
    }
}