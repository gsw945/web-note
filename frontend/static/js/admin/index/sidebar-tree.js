// 鼠标右键事件绑定
function onRightClick(event, treeId, treeNode) { 
    var tree_obj = $.fn.zTree.getZTreeObj(GLOBAL.TREE_ID);
    tree_obj.selectNode(treeNode); 
    //弹出菜单 
    var $menu = $('.menu-wrapper');
    setPosition($menu, event);
    if(treeNode) {
        // on node
        tree_obj.expandNode(treeNode, true, false, false, false);
    }
    else {
        // on tree
    }
    $menu.data('action-data', {
        'treeId': treeId,
        'treeNode': treeNode
    })
    $menu.show();
    console.log(treeId);
    console.log(treeNode);
}

// 数据过滤，并同时设置属性
function data_filter(treeId, parentNode, childNodes) {
    if(!childNodes || !childNodes.data) {
        return null;
    }
    childNodes = childNodes.data;
    var len = childNodes.length;
    for(var i = 0; i < len; i ++) {
        // childNodes[i].name = childNodes[i].name.replace(/\.n/g, '.');
        childNodes[i].isParent = childNodes[i].is_leaf == false;
    }
    // console.log(childNodes)
    return childNodes;
}

function zTreeBeforeAsync(treeId, treeNode) {
    // console.log(treeId);
    // console.log(treeNode);
    return true;
}

function zTreeOnAsyncSuccess(event, treeId, treeNode, msg) {
    // console.log(msg);
    if(!treeNode) {
        getDocList();
    }
}

function zTreeOnAsyncError(event, treeId, treeNode, XMLHttpRequest, textStatus, errorThrown) {
    console.log(XMLHttpRequest);
}

function zTreeOnClick(event, treeId, treeNode, clickFlag) {
    if(clickFlag == 1) {
        if(treeNode) {
            getDocList(treeNode.id);
        }
        else {
            getDocList();
        }
    }
};

// zTree 的参数配置，深入使用请参考 API 文档（setting 配置详解）
var setting = {
    view: {
        selectedMulti: false,
        showLine : true,
        expandSpeed: 500,
        dblClickExpand: true
    },
    async: {
        enable: true,
        url: '/api/folder/',
        type: 'get',
        // dataType: "text",
        dataType: "json",
        // contentType: "application/json",
        autoParam:['id=parent'],
        otherParam: {},
        dataFilter: data_filter
    },
    callback: {
        beforeRightClick: function(treeId, treeNode) {
            // 
            return true;
        },
        onRightClick: function(event, treeId, treeNode) {
            // console.log(treeNode ? treeNode.tId + ", " + treeNode.name : "isRoot");
            onRightClick(event, treeId, treeNode);
        },
        beforeAsync: zTreeBeforeAsync,
        onAsyncSuccess: zTreeOnAsyncSuccess,
        onAsyncError: zTreeOnAsyncError,
        onClick: zTreeOnClick
    },
    data: {
        key: {
            name: 'name'
        }
    }
};