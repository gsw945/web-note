/**
 * 获取鼠标点击(坐标)点
 */
function getMousePoint(event) { 
    var e = event || window.event; 
    var scrollX = document.documentElement.scrollLeft || document.body.scrollLeft; 
    var scrollY = document.documentElement.scrollTop || document.body.scrollTop; 
    var x = e.pageX || e.clientX + scrollX; 
    var y = e.pageY || e.clientY + scrollY; 
    return {
        'x': x,
        'y': y 
    }; 
}

/**
 * 设置菜单的位置
 */
function setPosition($menu, event) {
    var point = getMousePoint(event);
    var menu_w = $menu.outerWidth(true),
        menu_h = $menu.outerHeight(true),
        win_w = $(window).width(),
        win_h = $(window).height();
    // 防止超出可视范围而出现滚动条
    if(menu_h + point.y > win_h) {
        point.y = win_h - menu_h;
    }
    if(menu_w + point.x > win_w) {
        point.x = win_w - menu_w;
    }
    $menu.css({
        'left': point.x,
        'top': point.y
    });
}

function ajaxDeleteFolder(node) {
    var oid = node ? node.id : null;
    if(oid) {
        // 设置加载5秒
        var layer_args = {
            time: 5 * 1000
        };
        var index = layer.load(1, layer_args);
        $.ajax({
            url: '/api/folder/',
            type: 'DELETE',
            data: {
                id: oid
            },
            dataType: 'json',
            success: function(data) {
                if(data.error == 0) {
                    layer.msg(data.desc, {
                        icon: 1,
                        time: 2000 //2秒关闭
                    }, function(){
                        //do something
                    });
                    var tree_obj = $.fn.zTree.getZTreeObj(GLOBAL.TREE_ID);
                    tree_obj.removeNode(node, true);
                }
                else {
                    layer.msg(data.desc, {
                        icon: 0,
                        time: 2900 //2.9秒关闭
                    }, function(){
                        //do something
                    });
                }
            },
            error: function(xhr) {
                console.log(xhr.responseText);
            },
            complete: function() {
                layer.close(index);
            }
        });
    }
}

function ajaxCreateFolder(name, node) {
    var parent = node ? node.id : null;
    // 设置加载5秒
    var layer_args = {
        time: 5 * 1000
    };
    var index = layer.load(1, layer_args);
    var _data = {
        name: name
    };
    if(parent) {
        _data['parent'] = parent;
    }
    $.ajax({
        url: '/api/folder/',
        type: 'POST',
        data: _data,
        dataType: 'json',
        success: function(data) {
            if(data.error == 0) {
                var tree_obj = $.fn.zTree.getZTreeObj(GLOBAL.TREE_ID);
                var added = false;
                if(node) {
                    if(!node.isParent) {
                        tree_obj.expandNode(node, true, false, false, true);
                        node.isParent = true;
                        tree_obj.updateNode(node);
                    }
                    else {
                        if(!node.open) {
                            tree_obj.expandNode(node, true, false, false, true);
                            added = true;
                        }
                    }
                }
                if(!added) {
                    var new_node = data.data;
                    new_node.isParent = false;
                    tree_obj.addNodes(node, new_node);
                }
            }
            else {
                layer.msg(data.desc, {
                    icon: 0,
                    time: 2900 //2.9秒关闭
                }, function(){
                    //do something
                });
            }
        },
        error: function(xhr) {
            console.log(xhr.responseText);
        },
        complete: function() {
            layer.close(index);
        }
    });
}

function handleAction(action_data) {
    console.log(action_data);
    var tree_obj = $.fn.zTree.getZTreeObj(GLOBAL.TREE_ID);
    var node = action_data.treeNode;
    switch(action_data.action) {
        case 'create-folder':
            var layer_args = {
                title: '请输入目录名',
                value: '新建目录',
                formType: 0,
                success: function(layero, index){
                    action_data['menu'].hide();
                    layero.find(".layui-layer-input").select();
                }
            };
            layer.prompt(layer_args, function(value, index, elem){
                layer.close(index);
                ajaxCreateFolder(value, node);
            });
            break;
        case 'rename-folder':
            break;
        case 'delete-folder':
            action_data['menu'].hide();
            ajaxDeleteFolder(node);
            break;
        case 'create-article':
            break;
        case 'delete-article':
            break;
        case 'unselect-node':
            if(node) {
                tree_obj.cancelSelectedNode(node);
                action_data['menu'].hide();
                getDocList();
            }
            console.log(node)
            break;
        default:
            break;
    }
};

function hideMenu($menu) {
    $menu.removeData('action-data');
    $menu.hide();
}