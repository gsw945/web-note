# web note

*made with*:
- **backend**
    * [Bottle](https://bottlepy.org/) and [中文](https://bottle.zzir.cn/)
    * [ZODB](http://www.zodb.org/) and [ZEO](https://github.com/zopefoundation/ZEO)
- **frontend**
    * [jQuery](http://jquery.com/)
    * [zTree](http://www.treejs.cn/)
    * [layer](http://layer.layui.com/)

## refer
- [Building a Rest API with the Bottle Framework](https://www.toptal.com/bottle/building-a-rest-api-with-bottle-framework)
- [bottle framework with multiple files](https://stackoverflow.com/questions/11228153/bottle-framework-with-multiple-files)
- [ZODB 入门](https://www.ibm.com/developerworks/cn/aix/library/au-zodb/index.html)